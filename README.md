# be-subpages-widget-alphabetical-order

This is a Wordpress plugin. It requires the [BE Subpages Widget](https://wordpress.org/plugins/be-subpages-widget/) (or rather, it will have no effect without it).

By default, the BE Subpages Widget lists subpages in menu order.

This plugin forces the BE Subpages Widget to list subpages in alphabetical order.

Based on (i.e. shamelessly lifted from) [code provided by the BE Subpages Widget author himself](https://www.billerickson.net/code/subpages-widget-alphabetical-order/).