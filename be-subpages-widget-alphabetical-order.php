<?php

/*
Plugin Name: BE Subpages Widget Alphabetical Order
Plugin URI: https://bitbucket.org/austinjp/be-subpages-widget-alphabetical-order
Description: Force the excellent BE Subpages Widget to display subpages in alphabetical order.
Version: 0.0.1
Author: Austin Plunkett
Author URI: https://bitbucket.org/austinjp
*/

 /**
 * Alphabetically sort subpages
 * 
 * @link http://www.billerickson.net/code/subpages-widget-alphabetical-order/
 * @author Bill Erickson
 * 
 * @param array $args, query arguments
 * @return array $args
 */
function ea_alphabetically_sort_subpages( $args ) {
    $args['orderby'] = 'title';
    $args['order'] = 'ASC';
	unset($args['sort_column']); // This line is missing from Bill's original
    return $args;
}
add_filter( 'be_subpages_widget_args', 'ea_alphabetically_sort_subpages' );
